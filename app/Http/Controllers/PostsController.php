<?php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Account;
use App\Post;
use Abraham\TwitterOAuth\TwitterOAuth;

class PostsController extends BaseController
{
    public function update_tweets(){

        $new_tweets = [];

        $connection = new TwitterOAuth($_ENV['ConsumerKey'], $_ENV['ConsumerSecret'], $_ENV['AccessToken'], $_ENV['AccessTokenSecret']);

        $accounts = Account::all();

        foreach ($accounts as $account) {

            $last_id = Post::where('account_id', $account->account_id)->max('post_id');

            if($last_id){
                $timeline = $connection->get("statuses/user_timeline", ["screen_name" => $account->account_id, "since_id" => $last_id]);
            } else {
                $timeline = $connection->get("statuses/user_timeline", ["screen_name" => $account->account_id, "count" => 5]);
            }

            $i = 0;

            foreach ($timeline as $tweet) {

                $new_tweet = new Post();
                $new_tweet->account_id = $account->account_id;
                $new_tweet->post_id = $tweet->id;
                $new_tweet->text = $tweet->text;
                $new_tweet->created_at = $tweet->created_at;
                $new_tweet->favorite_count = $tweet->favorite_count;
                $new_tweet->retweet_count = $tweet->retweet_count;
                $new_tweet->save();
                $i++;
            }
            $account->posts_number += $i;
            $account->save();

            printf($account->account_id.' updated. '.$i. ' twetts added;</br>');
        }
    }

}
