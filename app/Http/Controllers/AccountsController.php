<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Account;
use App\Post;
use Abraham\TwitterOAuth\TwitterOAuth;


class AccountsController extends BaseController
{
    public function new_account(Request $request)
    {	
    	$refresh_interval 	= $request->input('refresh_interval');
    	$account_id			= $request->input('account_id');

    	if(Account::where('account_id',  $account_id)->first()){
    		return response()->json(['status' => 'error','error' => 'already in base']);
    	}

    	$connection = new TwitterOAuth($_ENV['ConsumerKey'], $_ENV['ConsumerSecret'], $_ENV['AccessToken'], $_ENV['AccessTokenSecret']);

	   $acc = $connection->get("users/show", ["screen_name" => $request->input('account_id')]);

	   	if ($connection->getLastHttpCode() == 200) {
	    	$new_account = new Account();
			$new_account->title = $acc->name;
			$new_account->account_id = $account_id;
			$new_account->refresh_interval = $refresh_interval;
	   	    $new_account->save();
	   
	   	    return response()->json(['status' => 'success','title' =>$acc->name]);
		} else {
	    	return response()->json(['status' => 'error','error' => $acc->errors[0]->message]);
		}	
	}


	 public function delete_account($account_id)
    {
    	$acc = Account::where('account_id', $account_id)->first();

    	if($acc){
    		$acc->delete();
    		return response()->json(['status' => 'success']);
    	} else {
    		return response()->json(['status' => 'error','error' =>'not found']);
    	}
    }


    public function edit_account($account_id, Request $request)
    {
    	$refresh_interval 	= $request->input('refresh_interval');
    	$acc = Account::where('account_id', $account_id)->first();

    	if($acc){
    		$acc->refresh_interval = $refresh_interval;
    		$acc->save();

    		return response()->json(['status' => 'success']);
    	} else {
    		return response()->json(['status' => 'error','error' =>'not found']);
    	}
    }
    

    public function list_of_accounts()
    {
 
    	$acc = Account::all();

    	if($acc){
    		return response()->json(['status' => 'success', 'list' =>$acc]);
    	} else {
    		return response()->json(['status' => 'error','error' =>'not found']);
    	}
    }


    public function list_of_posts($account_id, Request $request)
    {
		$acc = Account::where('account_id', $account_id)->first();
		if(!$acc){
    		return response()->json(['status' => 'error','error' =>'not found']);
    	}

    	if(isset($request->limit)){
    		$limit = $request->limit;
    	} else {
    		$limit = 100;
    	}

    	$posts = Post::where('account_id', $account_id)->take($limit)->get();
    	if($posts){
    		return response()->json(['status' => 'success', 'list' =>$posts]);
    	} else {
    		return response()->json(['status' => 'error','error' =>'not found']);
    	}
    }
 	
}
