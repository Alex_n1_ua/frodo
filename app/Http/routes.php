<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
	return view('index');
});
$app->get('/update', ['uses' => 'PostsController@update_tweets']);


$app->post('/accounts/new', ['uses' => 'AccountsController@new_account']);

$app->get('/accounts', ['uses' => 'AccountsController@list_of_accounts']);

$app->post('/accounts/{account_id}', ['uses' => 'AccountsController@edit_account']);

$app->post('/accounts/{account_id}/delete', ['uses' => 'AccountsController@delete_account']);

$app->get('/accounts/{account_id}/posts', ['uses' => 'AccountsController@list_of_posts']);
