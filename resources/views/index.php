<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title></title>
	<style>
		#dialog {
			display:none;
		}
		#dialog table {
			width: 100%;
		}
		#dialog table td {
			width: 50%;
			vertical-align: top;
		}
		#dialog textarea {
			white-space: pre-wrap;
			font-size:12px;
			width:100%;
			height:600px;

		}
		#dialog form input, #dialog form label, #dialog form button {
			display: block;
			width: 350px;
		}
		#dialog form label {
			font-weight: bold;
		}
		a:not(.api) {
			color: gray;
		}

		h2 {
			background-color: rgba(12, 130, 255, 0.12);
			padding:5px 5px 5px 15px;
			border-left: 4px solid #35a0fe;
		}
	</style>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/themes/smoothness/jquery-ui.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<script>
		var showResult = function(content) {
			var el = $("#dialog");
			el.find('#response').val(content);
		};
		var sendForm = function(form) {
			$.ajax(form.find('#formurl').val(), {
				type: form.attr('method'),
				cache: false,
				processData: false,
				data: form.serialize(),
				success: function(response, status, xhr, form) {
					showResult(xhr.responseText);
				},
				error: function(response, e, msg) {
					var message = [];
					if (response.status == 403) {
						message.push("User error");
						message.push(response.responseText);
					} else {
						message.push("System error");
						message.push('status: ' + response.status);
						message.push(msg);
					}
					showResult(message.join("\n"));
					console.log('fail', arguments);
				}
			});
		};
		var showForm = function(formData, method, url) {
			var el = $("#dialog");
			var form = el.find('#request');
			el.find('#response').val('');
			form.attr({
				method: method
			});
			form.children().remove();

			form.append($('<label>').html('URL'));
			var input = $('<input>');
			input.attr('id', 'formurl');
			input.val(url.split('?')[0]);
			form.append(input);

			var properties = Object.keys(formData);
			for (var f in properties) {
				var property = properties[f];

				form.append($('<label>').html(property));

				var input = $('<input>');
				input.attr('name', property);
				input.val(formData[property]);
				form.append(input);
			}
			form.append($('</br><button type="button" class="btn btn-success">').html('send ' + method).click(function() {sendForm(form);}))
			el.dialog({title: url, width: 800});
			if ((properties.length == 0) && (method == 'get')) {
				sendForm(form);
			}
		};
		var getParamsByHref = function(href) {
			var parser = parseHref(href);
			var paramsStr = parser.search.substr(1);
			var params = (paramsStr.length > 0) ? paramsStr.split('&') : {};
			var paramsObject = {};
			if (params) for (var i in params) {
				var p = params[i].split('=');
				paramsObject[p[0]] = p[1];
			}
			return paramsObject;
		};
		var getMethod = function(el) {
			var method = 'get';
			if (el.hasClass('post')) {
				method = 'post';
			} else if (el.hasClass('put')) {
				method = 'put';
			} else if (el.hasClass('delete')) {
				method = 'delete';
			}
			return method;
		};
		var parseHref = function(href) {
			var parser = document.createElement('a');
			parser.href = href;
			return parser;
		};
		$(function() {
			$('a.api').click(function(el) {
				var el = $(el.currentTarget);
				var href = el.attr('href');
				showForm(getParamsByHref(href), getMethod(el), href);
				return false;
			}).each(function(i, item) {
				var href = $(item).attr('href');
				var params = getParamsByHref(href);
				var method = getMethod($(item));

				/* есть ли заголовок, если нет - взять из ссылки */
				var title = $(item).parent().find('h3');
				if (title.length == 0) {
					$(item).before($('<h3>').html($(item).html()));
					$(item).html('тест');
				}

				var descr = $('<dl>');
				var dt = $('<dt>').html('URL');
				var dd = $('<dd>').html(href.split('?')[0]);
				descr.append(dt).append(dd);
				dt = $('<dt>').html('METHOD');
				dd = $('<dd>').html(method);
				descr.append(dt).append(dd);
				var properties = Object.keys(params);
				if (properties.length > 0) {
					dt = $('<dt>').html('DATA');
					dd = $('<dd>');
					var data = [];
					for (var i in properties) {
						data.push(properties[i])
					}
					dd.html(data.join('<br/>'));
					descr.append(dt).append(dd);
				}
				$(item).before(descr);
			});
		});
	</script>
</head>
<body>

<div id="dialog" title="dev tool">
	<table>
		<tr>
			<td>
				<form id="request">
					<label>hui</label>
					<input type="text" value="12" />
					<label>hui</label>
					<input type="text" value="12" />
					<button type="button">send</button>
				</form>
			</td>
			<td>
				<textarea id="response">some result</textarea>
			</td>
		</tr>
	</table>
</div>


<div class="container">
	<h2>API</h2>
	<h3>Accounts</h3>
	<ul><hr>
		<li><a class="api post" href="accounts/new?account_id=&refresh_interval=4">create</a></li><hr>
		<li><a class="api get"  href="accounts">list</a></li><hr>
		<li><a class="api post" href="accounts/:$account_id?refresh_interval=4">edit</a></li><hr>
		<li><a class="api post" href="accounts/:$account_id/delete">delete</a></li>
	</ul><hr>
	<h3>Posts</h3>
	<ul><hr>
		<li><a class="api get" href="accounts/:$account_id/posts?limit=100">list</a></li><hr>
	</ul>
</div>
</body>
</html>